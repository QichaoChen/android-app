package net.oschina.app.v2.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import com.tonlin.osc.happy.R;
import com.umeng.analytics.MobclickAgent;
import com.umeng.update.UmengUpdateAgent;
import com.umeng.update.UmengUpdateListener;
import com.umeng.update.UpdateResponse;
import com.umeng.update.UpdateStatus;

import net.oschina.app.v2.AppContext;
import net.oschina.app.v2.model.DailyEnglish;
import net.oschina.app.v2.model.Version;
import net.oschina.app.v2.utils.StringUtils;
import net.oschina.app.v2.utils.TDevice;

/**
 * 应用程序启动类：显示欢迎界面并跳转到主界面
 *
 * @author liux (http://my.oschina.net/liux)
 * @version 1.0
 * @created 2012-3-21
 */
public class SplashActivity extends Activity {

    private static final String SPLASH_SCREEN = "SplashScreen";
    private static final int SPLASH_DELAY_TIME = 1000;
    protected Version mVersion;
    protected boolean mShouldGoTo = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (TDevice.isServiceRunning(this, "net.oschina.app.v2.service.NoticeService")) {
            redirectTo();
            return;
        }
        AppContext.requestDailyEnglish();
        //checkUpdate();
        final View view = View.inflate(this, R.layout.v2_activity_splash, null);
        setContentView(view);
        DailyEnglish de = AppContext.getDailyEnglish();
        if (de != null) {
            TextView tvContent = (TextView) findViewById(R.id.tv_eng);
            tvContent.setText(de.getContent());
            TextView tvNote = (TextView) findViewById(R.id.tv_note);
            tvNote.setText(de.getNote());
        }

        if (mShouldGoTo) {
            redirectTo();
        }

        // 兼容低版本cookie（1.5版本以下，包括1.5.0,1.5.1）
        AppContext appContext = (AppContext) getApplication();
        String cookie = appContext.getProperty("cookie");
        if (StringUtils.isEmpty(cookie)) {
            String cookie_name = appContext.getProperty("cookie_name");
            String cookie_value = appContext.getProperty("cookie_value");
            if (!StringUtils.isEmpty(cookie_name)
                    && !StringUtils.isEmpty(cookie_value)) {
                cookie = cookie_name + "=" + cookie_value;
                appContext.setProperty("cookie", cookie);
                appContext.removeProperty("cookie_domain",
                        "cookie_name",
                        "cookie_value",
                        "cookie_version",
                        "cookie_path");
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(SPLASH_SCREEN); // 统计页面
        MobclickAgent.onResume(this); // 统计时长
        if (!mShouldGoTo) {
            redirectTo();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(SPLASH_SCREEN); // 保证 onPageEnd 在onPause
        MobclickAgent.onPause(this);
    }

    private void redirectTo() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.splash_fade_out, R.anim.main_fade_in);
                finish();
            }
        }, SPLASH_DELAY_TIME);
    }

    private void checkUpdate() {
        UmengUpdateAgent.setUpdateListener(new UmengUpdateListener() {
            @Override
            public void onUpdateReturned(int updateStatus,
                                         UpdateResponse updateInfo) {
                switch (updateStatus) {
                    case UpdateStatus.Yes: // has update
                        // mVersion = new Version(updateInfo);
                        mShouldGoTo = false;
                        UmengUpdateAgent.showUpdateDialog(getApplicationContext(), updateInfo);
                        break;
                    case UpdateStatus.No: // has no update
                        break;
                    case UpdateStatus.NoneWifi: // none wifi
                        break;
                    case UpdateStatus.Timeout: // time out
                        break;
                }
            }
        });
        UmengUpdateAgent.setUpdateAutoPopup(false);
        UmengUpdateAgent.update(getApplicationContext());
    }
}